/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.mycompany.micro.frameworks.spark1;
import static spark.Spark.*;
/**
 *
 * @author USER
 */
public class trytodospark {
     public static void main(String[] args) {   
        get("/hello",(req,res)->"Hello");
        post("/posts",(req,res)->"this is a post");
        put("/hello/:id",(req,res)->"This is a put");
        patch("/hello/:id",(req,res)->"This is an edit");
    }   
}
